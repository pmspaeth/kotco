# KOTCO - Composite Oriented Programming For Kotlin

Composite oriented programming is about tailoring classes by combining one or more mixins, possibly adding concerns, side effects and constraints. In addition, context-sensitive services can be added. The Kotco library provides all that for the Kotlin programmingh language.

Let's first talk about the installation and then the constituent parts.

## Installation

The current version 0.1.0 is provided as a JAR. Just download `kotco-0.1.0.jar` from the repository and add it to the classpath of your Kotlin project.

In addition, the following dependencies need to be fulfilled:

    <properties>
      <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
      <kotlin.version>1.2.50</kotlin.version>
    </properties>
    
    <dependenciees>
      <dependency>
        <groupId>org.jetbrains.kotlin</groupId>
        <artifactId>kotlin-stdlib</artifactId>
        <version>${kotlin.version}</version>
      </dependency>
      <dependency>
        <groupId>org.jetbrains.kotlin</groupId>
        <artifactId>kotlin-runtime</artifactId>
        <version>${kotlin.version}</version>
      </dependency>
      <dependency>
        <groupId>org.jetbrains.kotlin</groupId>
        <artifactId>kotlin-reflect</artifactId>
        <version>${kotlin.version}</version>
      </dependency>
      <dependency>
        <groupId>org.javassist</groupId>
        <artifactId>javassist</artifactId>
        <version>3.22.0-GA</version>
      </dependency>
      <dependency>
        <groupId>org.apache.commons</groupId>
        <artifactId>commons-configuration2</artifactId>
        <version>2.2</version>
      </dependency>
      <dependency>
        <groupId>junit</groupId>
        <artifactId>junit</artifactId>
        <version>4.12</version>
        <scope>test</scope>
      </dependency>
      ...
    </dependencies>

**A word about maturity: Despite the small version number Kotco can already in version 0.1 be used for production-level projects. The reason for that is that errors emerge fail-fast, since the composites get constructed during the first instantiation attempt and after that will run in a pretty stable manner**

If you have a Maven project, you can add the dependencies as shown. Otherwise download the libraries in the versions shown and add them to your classpath. 

To add a local copy of the Kotco JAR, say inside folder `extlibs`, to a Maven project, use the `system` scope:

    <dependency>
      <groupId>de.pspaeth</groupId>
      <artifactId>kotco</artifactId>
      <version>0.1.0</version>
      <scope>system</scope>
      <systemPath>${project.basedir}/extlibs/kotco-0.1.0.jar</systemPath>
    </dependency>

## Composite Interfaces

Composite interfaces describe software constituent parts in an abstract manner. They tell what things are about, but they don't tell how they perform their work. For example, a person as an abstract concept of a human being can be a composite interface. With Kotco, you just declare such an interface as in

    interface Person

## Behavior Interfaces

Behavior interfaces add behavior to composite interfaces. Returning to the previous example, a person might have a name and a year of birth. For the example we use two behavioral interfaces

    interface HasName {
      fun getName():String
    }

    interface HasYearOfBirth {
      fun getYearOfBirth():Int
    }

We can now extend the composite interface `Person` and write

    interface Person : HasName, HasYearOfBirth

We still didn't say a word of **how** the interfaces are to be implemented.

## Composites

Composites extend composite interfaces by the information on how things need to be done. This happens in a declarative manner, so an implementation is actually not yet provided. For example, we define a composite for the `Person` composite interface and write

    import de.pspaeth.kotco.framework.Mixins
    
    @Mixins(SimpleName::class, SimpleYearOfBirth::class)
    abstract class PersonTransient : Person


**Note**: Composites _can_ have a body. Such a body serves as a first 'direct' mixin. Due to some issues it is however recommended to not add bodies here. 

Composites serve as containers holding the final, instantiable classes. We talk about composite instantiation below.

## Mixins

Now that we declared composites to have mixins added to them, see above, we need to define the mixins. Mixins are normal, abstract classes which implement a part of a composite. For example, the mixins `SimpleName` and `SimpleYearOfBirth` could read:

    abstract class SimpleName : Person {
        override fun getName():String {
            // In reality request some backend service...
            return "John Doe"
        }
    }
    
    abstract class SimpleYearOfBirth : Person {
        override fun getYearOfBirth():Int {
            // In reality request some backend service...
            return 1997
        }
    }

**Note**: You could also write `abstract class SimpleName : HasName ...` and `abstract class SimpleYearOfBirth : HasYearOfBirth ...` - the framework wouldn't care.

**Note**: Keep mixins simple - especially avoid anonymous classes by using lambda calculus. In reality this is not a severe limitation, since a mixin might serve as a _facade_ and address any number of classes and Kotlin objects you like. For those referenced classes and objects you can then use all Kotlin language features.

## Transients and Entities

In composite oriented programming composites usually come in three flavors: 

+ **Transients** Those are simple composites with an application lifespan  
+ **Entities** Composites with an ID and an affinity to being persisted somehow. 
+ **Services** Context-sensitive composites which can be injected

While services get handled below, entities are not yet supported by Kotco. Until entities get supported, you can of course write your own persistence code for transients.

## Layers

Composites live inside layers. You need to have a layer to register and instantiate composites. This can be as easy as in

    import de.pspaeth.kotco.framework.Application
    import de.pspaeth.kotco.framework.Layer
    
    val layer = Application.layer("TheLayer").apply {
        transients(PersonTransient::class)
    }
    val personInstance = layer.newTransient(PersonTransient::class)

**Note**: It is possible to add a transient to several layers, but you then have different classes obeying the same interfaces though!

## Implementing Composites

We have already seen above that you can add mixins to composites via the `@Mixins()` annotation. There are three more constructs you can use: Concerns, Side Effects and Constraints. We list and describe all of them in the following sections.

### Mixins

As described above, mixins provide the basic implementation of behavioral interfaces to be used by composites. Mixins are abstract classes, and they can be declared to extend the behavioral interfaces in question, although it is not necessary.

    @Mixins(Mixin1::class, Mixin2::class)
    abstract class TheTransient : SomeBehavioralInterface1, SomeBehavioralInterface2
     
    abstract class Mixin1 : SomeBehavioralInterface1 {
        // ... [implementation of SomeBehavioralInterface1]
    }

    abstract class Mixin2 : SomeBehavioralInterface2 {
        // ... [implementation of SomeBehavioralInterface2]
    }

### Concerns

Concerns can be used to intercept the method calls implemented in mixins. You can change method call parameters, change method results, or even call other methods from the mixin or avoid the method call altogether. Extend `de.pspaeth.kotco.framework.Concern` and use `callOrig()` to call the original method, using the same parameters.

    import de.pspaeth.kotco.framework.Concern

    interface AddingInterface {
        fun add(a:Int,b:Int):Int
    }
    
    abstract class Mixin : AddingInterface {
        override fun add(a:Int,b:Int):Int = a + b
    }
    
    abstract class TheConcern : Concern, AddingInterface {
        override fun add(a:Int,b:Int):Int {
            val c = callOrig(a, b + 1) as Int
            return c + 1
        }
    }

    @Mixins(Mixin::class)
    @Concerns(TheConcern::class)
    abstract class TheComposite : AddingInterface
    
You can stack concerns, i.e. wrap a concern into another concern and so on. Just use `callOrig(...)` to call the next element in the chain.

    abstract class TheConcern1 : Concern, SomeBehavioralInterface {
        // implementation, use callOrig(...) to call methods from
        // SomeBehavioralInterface, e.g.
        fun someMethod(par:Int):Int {
            ...
            callOrig(par)
            ...
        }
    }

    abstract class TheConcern2 : Concern, SomeBehavioralInterface {
        // implementation, use callOrig(...) to call methods from
        // SomeBehavioralInterface or the wrapped concern, e.g.
        fun someMethod(par:Int):Int {
            ...
            callOrig(par)
            ...
        }
    }

    @Mixins(Mixin::class)
    @Concerns(TheConcern1::class, TheConcern2::class)
    abstract class TheConcern : SomeBehavioralInterface

Will symbolically allow to call `someMethodOfConcern2(someMethodOfConcern1(someMethod()))`

### Side Effects

Side effects are actions which do not interfere with functional aspects of an application. Prominent candidates are logging and monitoring, but you can use side effects for any crosscutting concern you like.

Side effects in Kotco are just abstract classes which formally implement interface methods:

    abstract class TheSideEffect : SomeBehavioralInterface {
        // implementation
        fun someMethod(par:Int):Int {
            //...
        }
    }

Side effects get then added to the composite via the `SideEffects` annotation:

    @Mixins(...)
    @Concerns(...)
    @Constraints(...) // see below
    @SideEffects(TheSideEffect::class, TheOtherSideEffect::class, ...)
    abstract class TheTransient : Interface


What the side effect method returns does not matter - the returned value will be ignored.

For the `Person` example you could write  

    abstract class NameSideEffect : HasName {
        override fun getName():String {
            // For example do some logging...
            // ...
            return "" // Nobody cares
        }
    }

    @Mixins(SimpleName::class, SimpleYearOfBirth::class)
    @SideEffects(NameSideEffect::class)
    abstract class PersonTransient : Person

Side effects in Kotco will be invoked _after_ the mixin's method execution.

### Constraints

Constraints are for checking the validities of parameters passed to interface methods. In Kotco, constraints are just abstract classes implementing behavior interface methods and throwing or not throwing an `de.pspaeth.kotco.framework.ConstraintViolationException`. For example, to check whether a parameter of some function invocation is not negative, you write

	interface i1 {
		fun a(par:Int):Int
		fun b(par:Int):Int
	}

	abstract class Constraint1 : i1 {
		override fun a(par:Int):Int {
			if(par < 0) throw ConstraintViolationException("Param must be >= 0")
			return 0 // nobody cares
		}
	}	

You then add constraints to composites via

		@Mixins(...)
		@Constraints(Constraint1::class)
		abstract class TheTransient : i1
		
As for side effects, the values a constraint method returns will be ignored, so you can write anything you like here.

## Properties

While you can add `vals` and `vars` at will to mixins, concerns, side effect and constraints (name clashes forbidden though),  Kotco allows for a more versatile variables construct named "properties". To declare properties, you add to mixins, concerns, side effect and constraints, or any interface, something like

    abstract fun propertyName(): Property<Int>

where instead of `propertyName` you can use any identifier you like and instead of `<Int>` you can use any of: 

* `<Int>`, `<Long>`, `<Short>`, `<Byte>` 
* `<Double>`, `<Float>`
* `<Char>`, `<String>`
* `<Boolean>` 
* `<Set>`, `<Map>`, `<List>` (any inner type)  
* `<Any>` (custom Object)

To use a property you write

    propertyName().get()
    
and

    propertyName().set( ... )

to get and set the property. To have a property initialized with defaults, add `@Defaults` as in

    import de.pspaeth.kotco.framework.Property
    import de.pspaeth.kotco.framework.Defaults
    
    [...]
    @Defaults abstract fun propertyName(): Property<Int>
    
This works for all property types, where for `<Any>` a blank objects gets passed in.

Properties need a storage. There is currently one storage built in. To use it, add `PropertyMapHandler` to the mixins list, as in

    import de.pspaeth.kotco.framework.Mixins
    
    @Mixins(PropertyMapHandler::class, [...])
    abstract class PersonTransient : Person

The `Person` example using properties thus reads

    interface HasName {
        fun name():Property<String>
    }

    interface HasYearOfBirth {
        fun yearOfBirth():Property<Int>
    }

    interface Person : HasName, HasYearOfBirth

    @Mixins(PropertyMapHandler::class, SimpleName::class, SimpleYearOfBirth::class)
    abstract class PersonTransient : Person
    
    abstract class SimpleName : Person {
        @Defaults override fun name():Property<String>
    }
    
    abstract class SimpleYearOfBirth : Person {
        @Defaults override fun yearOfBirth():Property<Int>
    }
    
**Note**: Don't wonder you see no implementation here for properties - the framework and the property mixin take care of that.


## Services

Services are composites which can be injected at _runtime_ into mixins, side effects, concerns, constraints and other services.

To define a service do the same as for a transient, but implement the `ServiceComposite` interface:

	interface s1 {
		fun run(par: Int):String
	}

	class ServiceMixin : s1 {
		override fun run(par: Int):String {
			return "Service run(${par})"
		}
	}

	@Mixins(ServiceMixin::class)
	abstract class ServiceComp : ServiceComposite, s1

To use it first register it inside the layer:

	val layer = Application.layer("TheLayer")
	layer.transients(...)
	layer.services(ServiceComp::class)

and the apply the `de.pspaeth.kotco.framework.Service` annotation inside any mixin, concerns, side effect or constraint:

    @Service lateinit var servc : ServiceComp
    
You can then just invoke any service method on this field.

**Note**: The framework performs the injection _after_ instantiation, but the Kotlin compiler doesn't know about that, which is why we must allow lateinit and nullability here. 

**Note**: You can also use interface for the `@Service` annotation. The framework then tries to find a match inside the set of registered services in the current layer. This allows for some kind of service polymorphism on a composite level.

## Instantiation of Composites

As seen before in the examples, instantiation of composites happens using a `Layer` as in 

	val layer = Application.layer("TheLayer")
	layer.transients(...) // register transients here
	layer.services(...)   // register services here
	...
	val trans1 = layer.newTransient(TheTransient::class)
	...

The `newTransient()` method is clever enough to allow for using composite interfaces as well, but only if there is only one possible composite using that interface given the layer.

## Injections

The following injections, all inside package `de.pspaeth.kotco.framework`, are possible inside mixin, concern, side effect and constraint classes.

### `@Service`
Use this on a field to instantiate and inject a service composite. Which service composite to select gets derived by the type of the field. You can use an interface here as well, but the composite in the supertype hierarchy of that interface needs to be unique inside the current layer. Example: `@Service lateinit var servc : ServiceComp`

Services can be injected several times from different mixins. Given the same name and type, the service will be injected only once and the formal ambiguity ignored.

### `@This`
While for addressing the composite itself you can always use the `this` identifier provided by the language, using the `@This` annotation helps to upcast a field. So for example inside

    interface HasName { fun getName():String }
    interface Person : HasName
    
    abstract class SimpleName {
        @This lateinit var thePerson:Person 
        fun getName():String = "John from inside ${thePerson}" 
    }
    
    @Mixins(SimpleName::class)
    abstract class PersonComposite
    
event though the mixin `SimpleHasName` per se does not know about the composition and by compilation cannot acquire a reference to `Person`, the `@This` will inject it at instantiation time. 

### `@Structure`
You can use this to inject the current layer. You can for example let composites register and instantiate other composites. Synopsis:

    @Structure lateinit var theLayer:Layer


## Enablers

In order to simplify composite construction, two annotations have been added to the framework: `@Enable` and `@Enabler`. They allow for injection into objects used by composites. They idea goes precisely as follows: add `@Enable` to the mixin class, using a Kotlin object class as annotation argument:

    @Enable(ToBeEnabled::class)
    abstract class SomeMixinClass ... { ... } 

Then, inside the _enabled_ object, most injections apply, as in

    object ToBeEnabled {
        @Enabler lateinit var someThisRef: SomeThisRefClass = null
        @Structure lateinit var theLayer: Layer = null
        @Service lateinit var someServiceRef: SomeServiceRefClass = null
        ... 
    }
    
Where you can't use `@This`, but as a substitute may provide a field annotated with `@Enabler` to have a reference to the class which declared `@Enable`.
    
So you don't have to forward _This_, structure or services to the object in order to make it usable from inside the mixin:

    @Enable(ToBeEnabled::class)
    abstract class SomeMixinClass ... {
        ToBeEnabled.doWhatEver()  // <- has everything injected automatically 
        ...
    } 


## Issues

A couple of things need to be taken care of for using Kotco in the current version:

### Avoid Name Clashes For Fields

Currently something like a namespace for fields does not exist. For example, you cannot write

    @Mixins(SimpleName::class, SimpleYearOfBirth::class)
    abstract class PersonTransient : Person

    abstract class SimpleName : Person {
        val a = 7
        ...
    }
    
    abstract class SimpleYearOfBirth : Person {
        val a = 8
        ...
    }

since `a` is used by both mixins for _one_ composite. 

You can however use the same name for multiply injected fields, so there is no problem with

    @Mixins(SimpleName::class, SimpleYearOfBirth::class)
    abstract class PersonTransient : Person

    abstract class SimpleName : Person {
        @This var pers:Person? = null
        ...
    }
    
    abstract class SimpleYearOfBirth : Person {
        @This var pers:Person? = null
        ...
    }

### Anonymous Inner Classes

Do not use closures in mixins, side effects, concerns, constraints and services. You can however use closures and other anonymous inner classes in referred-to classes at will.

### Classes Inside Functions

While Kotlin in general allows class declarations inside functions, this does not always work for Kotco. As a general rule of thumb avoid such constructs. Classes inside other _classes_ are no problem though.