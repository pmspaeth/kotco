package de.pspaeth.kotco.framework.bytecode

import de.pspaeth.kotco.framework.Defaults
import de.pspaeth.kotco.util.erasedType
import javassist.CtClass
import javassist.CtNewMethod
import javassist.bytecode.Descriptor
import javassist.bytecode.SignatureAttribute
import java.lang.reflect.Modifier
import kotlin.reflect.KClass
import kotlin.reflect.jvm.javaType
import kotlin.reflect.jvm.jvmName


class PropertiesBuilder(val component:CtClass) {
	companion object {
		val emptyConstructorDescriptor = Descriptor.ofConstructor(arrayOf())
	}

	/**
	 * Properties can get their implementation directly from declared interfaces. This is different
	 * from other methods and fields, which can get their implementation only by mixins. For interface
	 * provided properties, we just copy the property to the component class and let method
	 * handleProperties() do the rest.
	 */
	fun <T:Any> handleDirectProperties(declaring:KClass<T>) {
		//println("|! " + declaring.supertypes)
		declaring.supertypes.map { st -> st.javaType.erasedType().kotlin }.filterNot{ it == Any::class}.forEach{ clazz ->
			val ctClass = BytecodeUtil.getCtClassByName(clazz.jvmName)!!
			ctClass.declaredMethods.
				filter{ m -> m.returnType.name.endsWith(".framework.Property") }.
				filter{ m -> m.modifiers and Modifier.ABSTRACT != 0 }.
				forEach { m ->
					if( ! component.declaredMethods.map { mm -> mm.name }.any { n -> n == m.name } ) {
						val methodCopy = CtNewMethod.copy(m, component, null)
						m.annotations.takeIf { arr -> arr.isNotEmpty() }?.run { MyAnnotations.transportMethodAnnotations(methodCopy, this) }
						//println("Adding abstract method ${m} from ${clazz} to ${compClass}")
						component.addMethod(methodCopy)
					}
			}
		}
	}
	
	/**
	 * Go through all abstract property methods and implement them calling the property handler
	 */
	fun handleProperties() {
		component.declaredMethods.
				filter{ m -> m.returnType.name.endsWith(".framework.Property")
						&& m.name != "_theProperty" && m.name != "_thePropertyInit" }.
				filter{ m -> m.modifiers and Modifier.ABSTRACT != 0 }.
				forEach { m ->
					val meth = SignatureAttribute.toMethodSignature(m.genericSignature?:m.signature)
					val rt = meth.returnType.toString()
					m.setBody("""{
							return _theProperty("${m.name}", "${rt}");
					}""")
					m.annotations.find { ann -> ann is Defaults }?.run {
						// The @Defaults annotation at a property method means the property must be
						// initialized. We do this in the constructor
						component.getConstructor(emptyConstructorDescriptor).insertAfter(
			                "_thePropertyInit(\"${m.name}\", \"${rt}\");"
		                )
					}
		}
	}
}