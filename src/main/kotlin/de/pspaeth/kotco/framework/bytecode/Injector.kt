package de.pspaeth.kotco.framework.bytecode

import de.pspaeth.kotco.framework.Enabler
import de.pspaeth.kotco.framework.Layer
import de.pspaeth.kotco.framework.Service
import de.pspaeth.kotco.framework.Structure
import de.pspaeth.kotco.framework.This

class Injector {
	fun handleInjections(obj:Any, layer:Layer) {
		handleInjections(obj, layer, null)
	}
	
	fun handleInjections(obj:Any, layer:Layer, enabler:Any?) {
		// This looks a little bit complicated, but the members accessor of Kotlin
		// has a problem with static fields (a bug?)
		obj::class.java.declaredFields.forEach { f ->
			f.annotations.forEach { ann ->
				when(ann) {
					is Service -> {
						val cl = f.type.kotlin
						val newi = layer.newService(cl)
						f.set(obj, newi)
					}
					is Structure -> {
						val cl = f.type.kotlin
						if( cl == Layer::class ) {
							f.set(obj, layer)
						}
					}
					is This -> {
						f.set(obj, obj)
					}
					is Enabler -> {
						f.set(obj, enabler)						
					}
				}				
			}
		}
	}
}