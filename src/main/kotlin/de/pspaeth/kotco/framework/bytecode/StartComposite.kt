package de.pspaeth.kotco.framework.bytecode

import de.pspaeth.kotco.framework.Layer
import javassist.ClassPool
import javassist.CtClass
import javassist.CtField
import javassist.CtNewConstructor
import javassist.CtNewMethod
import javassist.Modifier
import javassist.bytecode.Descriptor
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName

class StartComposite {
	companion object {
		val pool = ClassPool.getDefault()
		val emptyConstructorDescriptor = Descriptor.ofConstructor(arrayOf())
		val pckg = this::class.java.`package`.name
		val frameworkPackage = this::class.java.`package`.name.replace(Regex("\\.[^.]*$"),"")
	}

	/**
	 * Build the composite basic, add a static "_layer" field, add a default constructor
	 * with injection processor, add an abstract _theProperty() for property processing,
	 * add an empty _creationDone() for creation listeners and let it be called from the
	 * constructor. Call _newComposite() and provide a template for it, so mixins can use it
	 */
	fun <T:Any> prepareComposite(declaring:KClass<T>, layerName:String):CtClass {
		val tr = BytecodeUtil.getCtClassByName(declaring.jvmName)
	    var compClass = pool.makeClass(declaring.jvmName+"__tr__" + layerName.replace(Regex("[^A-Za-z0-9]"),"_"))
		compClass.setSuperclass(tr)
					
		// Add a static layer field
		val layerClass = BytecodeUtil.getCtClassByName(Layer::class.jvmName)?: throw RuntimeException("Internal error")
		val layerField = CtField(layerClass, "_layer", compClass).apply {
			modifiers = modifiers or Modifier.STATIC or Modifier.PUBLIC
		}
		compClass.addField(layerField)

		// Provide default constructor
		val cons = CtNewConstructor.defaultConstructor(compClass)
		compClass.addConstructor(cons)
		
		// Add the injection processor to the constructor
		compClass.getConstructor(emptyConstructorDescriptor).insertAfter(
			"new " + pckg + ".Injector().handleInjections(this,_layer);"
		)
		
		// Add creation listeners as a template method, and to the constructor
	    val creationListeners = CtNewMethod.make("public void _creationDone(){}", compClass)
		compClass.addMethod(creationListeners)
		compClass.getConstructor(emptyConstructorDescriptor).insertAfter(
			"_creationDone();"
		)
		
		// Add an abstract property handler
	    val handlerMeth = CtNewMethod.make("public abstract ${frameworkPackage}.Property _theProperty(java.lang.String s1, java.lang.String s2);", compClass).apply { modifiers = modifiers or Modifier.ABSTRACT  }
		compClass.addMethod(handlerMeth)
	    val handlerInitMeth = CtNewMethod.make("public abstract ${frameworkPackage}.Property _thePropertyInit(java.lang.String s1, java.lang.String s2);", compClass).apply { modifiers = modifiers or Modifier.ABSTRACT  }
		compClass.addMethod(handlerInitMeth)
				
		return compClass
	}
}