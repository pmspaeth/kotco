package de.pspaeth.kotco.framework.bytecode

import de.pspaeth.kotco.framework.Constraints
import javassist.CtClass
import javassist.CtNewMethod
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName

class ConstraintsBuilder(val component:CtClass) {
	/**
	 * Using the composite declaration, add the constraints given in the Constraints
	 * annotation. The constaint methods get inserted into the class with a
	 * "_CO1" , "_CO2", ... ending and a call to them gets added at the beginning of
	 * the constrained methods.
	 *
	 * Note that fields in constraints cannot be handled.
	 */
	fun <T:Any> handleConstraints(declaring:KClass<T>) {
		var cnt = 0
		declaring.java.getAnnotation(Constraints::class.java)?.clazzes?.forEach { kclazz ->
			cnt++
			val consClass = BytecodeUtil.getCtClassByName(kclazz.jvmName)?: throw RuntimeException("Internal error")
			
			BytecodeUtil.annonymousClassesNotPossible("constraint", consClass)
			
			consClass.getDeclaredMethods().forEach { m ->
				val methodName = m.name
				val parTypes = m.parameterTypes
				component.getDeclaredMethod(methodName, parTypes)?.run {
					val seMethodCopy = CtNewMethod.copy(m, component, null).apply {
						name = methodName + "_CO${cnt}"
					}
					component.addMethod(seMethodCopy)
					insertBefore(methodName + "_CO${cnt}($$);")
				}
			}
			consClass.getDeclaredFields().forEach { _ ->
				throw RuntimeException("In ${kclazz} - Fields are not allowed in constraints")
			}
		}
	}
}