package de.pspaeth.kotco.framework.bytecode

import de.pspaeth.kotco.framework.Enable
import de.pspaeth.kotco.framework.Mixins
import javassist.CtClass
import javassist.bytecode.Descriptor
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName


class Enablers(val component:CtClass) {
	companion object {
		val emptyConstructorDescriptor = Descriptor.ofConstructor(arrayOf())
	}

	fun	<T:Any> handleReferencedClasses(clazz:KClass<T>) {
		clazz.java.getAnnotation(Mixins::class.java)?.clazzes?.forEach { kclazz ->
			kclazz.annotations.filter{ann->ann is Enable}.forEach{ ann->
				(ann as Enable).clazzes.forEach { annClass ->						
					// Add the injection processor to the constructor
					val thisPackage = this::class.java.`package`.name
					component.getConstructor(emptyConstructorDescriptor).insertAfter(
						"new " + thisPackage + ".Injector().handleInjections(${annClass.jvmName}.INSTANCE,_layer, this);"
					)
				}
			}
		}
	}
}