package de.pspaeth.kotco.framework.bytecode

import de.pspaeth.kotco.framework.Concerns
import javassist.CtClass
import javassist.CtField
import javassist.CtMethod
import javassist.CtNewMethod
import javassist.CtPrimitiveType
import javassist.Modifier
import javassist.bytecode.Descriptor
import javassist.expr.ExprEditor
import javassist.expr.MethodCall
import org.jetbrains.annotations.NotNull
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName


class ConcernsBuilder(val component:CtClass) {
	companion object {
		val emptyConstructorDescriptor = Descriptor.ofConstructor(arrayOf())
	}

	/**
	 * Using the composite declaration, add the concerns given in the Concerns
	 * annotation.
	 *
	 * All the original mixed-in methods which have a counterpart in the concern class get
	 * moved to methods with a "_CON$X" at the end where the "X" is the first free number
	 * not corresponding to an already moved method. All the other methods from the
	 * concerns just get copied.
	 *
	 * If a method was moved, the original method gets reconstructed by the contents of
	 * the concern method, with calls "callOrig()" replaced by calls to the moved-away
	 * method.
	 *
	 * Concerns are allowed to transport fields into the composite, but it is a
	 * limitation that different concerns and mixins may not use fields of the same name.
	 *
	 * The constructor from each concern, it is responsible for initializing vals and vars,
	 * gets transported to the composite and it gets called at the end of the
	 * composite's no-arg constructor.
	 */
	fun <T:Any> handleConcerns(declaring:KClass<T>) {
		var cnt = 0
		declaring.java.getAnnotation(Concerns::class.java)?.clazzes?.forEach { kclazz ->
			cnt++
			val concernClass = BytecodeUtil.getCtClassByName(kclazz.jvmName)?: throw RuntimeException("Internal error")
			
			BytecodeUtil.annonymousClassesNotPossible("concern", concernClass)

			concernClass.getDeclaredMethods().forEach { m ->
				// All methods of the orignal class to be concerned get moved to
				// methods with names "`name`_CON$"
				val parTypes = m.getParameterTypes()
				val origMethod = BytecodeUtil.getMethod(component, m.name, parTypes)
				
				if(origMethod == null) {
					// These are concern methods not affecting transient class methods - we just copy them
					val me = CtNewMethod.copy(m, component, null)
					component.addMethod(me)					
				} else{
					component.removeMethod(origMethod) // make place
					
					val movedToBase = origMethod.name + "_CON$"
					var origMethodStr:String?
					var cnt2 = 0
					while(true) { cnt2++
						origMethodStr = movedToBase + cnt2
						val m2 : CtMethod? = BytecodeUtil.getMethod(component, origMethodStr, parTypes)
						if(m2 == null) {
							// It is not already there
							origMethod.name = origMethodStr
							component.addMethod(origMethod) // save under new name
							break
						}
					}
						
					// take the method from the concern and replace the inside 'callOrig()' method call by a call to the just saved method
					val me = CtNewMethod.copy(m, component, null)
					me.instrument( object : ExprEditor() {
					    override fun edit( m:MethodCall ) {
					        if (m.methodName == "callOrig") {
								val retType = origMethod.returnType
								if(!retType.isPrimitive) {
									m.replace("{ \$_ = new Integer(${origMethodStr}(7)); }");
								} else {
									val classArrStr = parTypes.map { ctcl ->
										when(ctcl) {
											CtPrimitiveType.intType -> "int.class"
											CtPrimitiveType.longType -> "long.class"
											CtPrimitiveType.shortType -> "short.class"
											CtPrimitiveType.booleanType -> "boolean.class"
											CtPrimitiveType.charType -> "char.class"
											CtPrimitiveType.byteType -> "byte.class"
											CtPrimitiveType.floatType -> "float.class"
											CtPrimitiveType.doubleType -> "double.class"
											else -> ctcl.name + ".class"
										}
									}.joinToString(",")
																		
									m.replace("{" +
											"java.lang.reflect.Method m = getClass().getDeclaredMethod(\"${origMethodStr}\",new Class[]{${classArrStr}});" +
											"\$_ = m.invoke(this, $1 );" +
											" }")								
								}
							}
					    }
					})
					component.addMethod( me )						
				}
			}
			
			concernClass.getDeclaredFields().forEach { f ->
				val fieldName = f.name
				val fieldModifiers = f.modifiers
				val fieldType = f.type
				val fieldAnnots = f.annotations.filterNot{ ann -> ann is NotNull}
				if(BytecodeUtil.getField(component, fieldName) != null) {
					throw RuntimeException("Field ${fieldName} from Concern ${kclazz} already exists in the transient")
				}
				val newField = CtField(fieldType, fieldName, component).apply {
					modifiers = fieldModifiers
					MyAnnotations.transportAnnotations(fieldInfo, fieldAnnots)						
				}
				if(fieldModifiers and Modifier.STATIC != 0 && fieldModifiers and Modifier.FINAL != 0) {
					val const = f.getConstantValue()
					val initializer = when(const) {
						is String -> "\"${const}\""
						is Char -> "'${const}'"
						else -> "${const}"
					}	
					component.addField(newField, initializer)						
				} else {
					component.addField(newField)
				}
			}
			
			// Make the concern's constructor available (we need it to initialize fields)
			val artfcConstr = (concernClass.name + "_${cnt}").replace(Regex("\\W"),"_")
			val constrAsMethod = concernClass.getConstructor(emptyConstructorDescriptor).toMethod(artfcConstr, component)
			component.addMethod(constrAsMethod)
			component.getConstructor(emptyConstructorDescriptor).insertAfter(artfcConstr+"();") 
		}
	}
}