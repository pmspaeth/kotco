package de.pspaeth.kotco.framework.bytecode

import javassist.CtClass
import javassist.CtField
import javassist.NotFoundException
import javassist.bytecode.Descriptor
import org.jetbrains.annotations.NotNull
import java.lang.reflect.Modifier
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName


object Fields {
	val emptyConstructorDescriptor = Descriptor.ofConstructor(arrayOf())

	fun passFields(fromClass:KClass<*>, toClass:CtClass, cnt:Int) {
		val theCtClass = BytecodeUtil.getCtClassByName(fromClass.jvmName)?: throw RuntimeException("Internal error")

		// make sure we have no vars in the companion				
		val companionName = fromClass.jvmName + "\$Companion"
		BytecodeUtil.getCtClassByName(companionName)?.run {
			if(this.getDeclaredMethods().any({ m ->
				m.modifiers and Modifier.FINAL == 0
			    || !m.name.startsWith("get")
			})) {
				throw RuntimeException("Companion var fields not allowed, in mixed-in class ${theCtClass.name}")
			}
		}
		
		theCtClass.getDeclaredFields().forEach { f ->
			val fieldName = f.name
			val fieldModifiers = f.modifiers
			val fieldType = f.type
			val fieldAnnots = f.annotations.filterNot{ ann -> ann is NotNull}
			
			BytecodeUtil.getField(toClass, fieldName)?.run {
				// If the field has the same name and type and got injected by one of the annotations
				// @This, @Service, @Structure, overriding just gets ignored. Otherwise we
				// throw an exception
				val myAnnots = annotations.filterNot{ ann -> ann is NotNull}
				val wasInjected = isInjecting(myAnnots)
				if(!wasInjected &&
						(type != fieldType ||
						myAnnots.size != fieldAnnots.size ||
						myAnnots.toHashSet() != fieldAnnots.toHashSet()) ) { 
				    throw RuntimeException("Field ${fieldName} from Mixin ${fromClass} already exists in the transient")
				} else {
					try{ toClass.removeField(this) }catch(e:NotFoundException){}
				}
			}
			val newField = CtField(fieldType, fieldName, toClass).apply {
				modifiers = fieldModifiers
				MyAnnotations.transportAnnotations(fieldInfo, fieldAnnots)						
			}
			if(fieldModifiers and Modifier.STATIC != 0 && fieldModifiers and Modifier.FINAL != 0) {
				val const = f.getConstantValue()
				val initializer = when(const) {
					is String -> "\"${const}\""
					is Char -> "'${const}'"
					else -> "${const}"
				}	
				toClass.addField(newField, initializer)
			} else {
				toClass.addField(newField)
			}
		}
		
		// Make the mixin's constructor available (we need it to initialize fields)
		val artfcConstr = (theCtClass.name + "_${cnt}").replace(Regex("\\W"),"_")
		try {
			val constrAsMethod = theCtClass.getConstructor(emptyConstructorDescriptor).toMethod(artfcConstr, toClass)
			toClass.addMethod(constrAsMethod)
			toClass.getConstructor(emptyConstructorDescriptor).insertAfter(artfcConstr+"();")
		}catch(e:NotFoundException) {
			// intentionally empty
		}
	}		
}