package de.pspaeth.kotco.framework.bytecode

import de.pspaeth.kotco.framework.Mixins
import javassist.ClassMap
import javassist.ClassPool
import javassist.CtClass
import javassist.CtNewMethod
import javassist.Modifier
import javassist.bytecode.Descriptor
import javassist.expr.ExprEditor
import javassist.expr.MethodCall
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName

class BodyAndMixinsBuilder(val component:CtClass) {
	companion object {
		val pool = ClassPool.getDefault()
		val emptyConstructorDescriptor = Descriptor.ofConstructor(arrayOf())
	}

	/**
	 * Using the composite declaration, add the mixins given in the Mixins
	 * annotation. Mixins are allowed to have "val" fields in the companion, other
	 * companion elements are not allowed.
	 *
	 * Mixins are allowed to transport fields into the composite, but it is a
	 * limitation that different mixins may not use fields of the same name.
	 *
	 * The method a mixin implements must not already exist in the composite, for example
	 * because other mixins already did that.
	 *
	 * The constructor from each mixin, it is responsible for initializing vals and vars,
	 * gets transported to the composite and it gets called at the end of the
	 * composite's no-arg constructor.
	 */
	fun <T:Any> handleBodyAndMixins(declaring:KClass<T>) {
		// Handle the composite declaration members like a first mixin
		val theDeclaringClass = BytecodeUtil.getCtClassByName(declaring.jvmName)?: throw RuntimeException("Internal error")			
		BytecodeUtil.annonymousClassesNotPossible("declaring", theDeclaringClass)
		
		passMembers(declaring, component, 1)
		
		var cnt = 1
		declaring.java.getAnnotation(Mixins::class.java)?.clazzes?.forEach { kclazz ->
			cnt++
			val theMixinClass = BytecodeUtil.getCtClassByName(kclazz.jvmName)?: throw RuntimeException("Internal error")
			BytecodeUtil.annonymousClassesNotPossible("mixin", theMixinClass)
			passMembers(kclazz, component, cnt)
			
			// The way annonymous classes get handled inside Kotlin, for example for lambda functions, is complex and brittle
			// We don't do it for now
			//theMixinClass.getRefClasses()?.filter { s -> s.toString().matches(Regex(".*\\$\\d+$")) }?.forEach { s ->
			//	copyAnnonClass(s as String, component)
    		//}
		}						
	}
	
//	private fun copyAnnonClass(aCl:String, comp:CtClass) {
//		// aCl f.ex. "de.pspaeth.musikosmos.oeuvre002.AA$DEBUG$x$1"
//		val theAnnonClass = BytecodeUtil.getCtClassByName(aCl)?: throw RuntimeException("Internal error")
//		val compName = comp.name // f.ex. "de.pspaeth.musikosmos.oeuvre002.implementation.localizedtones.LocalizedToneComposite__tr__LocalizedTonesUniverse"
//		println("§§ " + compName)
//		val i1 = aCl.indexOf("$")
//		val nn = aCl.substring(i1)
//		val movedName = compName + nn
//		println("" + aCl + " moved to: " + movedName)
//		val renamed = pool.getAndRename(aCl, movedName)
//		//renamed.toClass()
//		
//		// TODO replace all calls to aCl members by calls to "renamed" members
//		comp.refClasses.forEach { it ->
//			println("    " + it)
//		}
//		val nameMap = ClassMap()
//		nameMap[aCl] = movedName
//		
//		//comp.replaceClassName(nameMap) <- does not work s expected
//		
//		//val cconv = CodeConverter()
//		//cconv.replaceNew(theAnnonClass, renamed)
//		//comp.instrument(cconv)
//		
//		val conv = object : ExprEditor() {
//			override fun edit(mc:MethodCall) {
//				println("    mc: " + mc.className + " " + mc.method)
//				if(mc.className.toString() == "kotlin.jvm.functions.Function1") {
//					println("      1: " + mc.javaClass)
//					println("      2: " + mc.signature)
//					println("      3: " + mc.enclosingClass)
//				}
//			}
//		}
//		//comp.instrument(conv)
//		comp.methods.forEach { m ->
//			println(m.toString() + " -->")
//			m.instrument(conv)
//		}
//		
//		//		comp.declaredMethods.forEach { m ->
//		//			val cconv = CodeConverter()
//		//			cconv.replaceNew(theAnnonClass, renamed)
//		//			//cconv.redirectMethodCall()
//		//			comp.instrument(cconv)
//		//			m.instrument(cconv)
//		//			
//		//			m.
//		//		}
//		
//		comp.refClasses.forEach { it ->
//			println(">   " + it)
//		}
//		
//	}

	private fun passMembers(fromClass:KClass<*>, toClass:CtClass, cnt:Int) {
		val theCtClass = BytecodeUtil.getCtClassByName(fromClass.jvmName)?: throw RuntimeException("Internal error")

		// make sure we have no vars in the companion				
		val companionName = fromClass.jvmName + "\$Companion"
		BytecodeUtil.getCtClassByName(companionName)?.run {
			if(this.getDeclaredMethods().any({ m ->
				m.modifiers and Modifier.FINAL == 0
			    || !m.name.startsWith("get")
			})) {
				throw RuntimeException("Companion var fields not allowed, in mixed-in class ${theCtClass.name}")
			}
		}
		
		theCtClass.getDeclaredMethods().forEach { m ->
			val theCopyMethod = CtNewMethod.copy(m, toClass, null)
			m.annotations.takeIf { arr -> arr.isNotEmpty() }?.run { MyAnnotations.transportMethodAnnotations(theCopyMethod, this) }
			if(m.genericSignature != null) theCopyMethod.genericSignature = m.genericSignature
			val theMethodPars = theCopyMethod.parameterTypes
			
			val methInComp = BytecodeUtil.getMethod(toClass,m.name,theMethodPars)
			if(methInComp != null) {
				toClass.removeMethod(methInComp)
			}
			toClass.addMethod(theCopyMethod)
		}
									
		Fields.passFields(fromClass, toClass, cnt)
	}		


}