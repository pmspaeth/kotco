package de.pspaeth.kotco.framework.bytecode

import de.pspaeth.kotco.framework.Defaults
import de.pspaeth.kotco.framework.Service
import de.pspaeth.kotco.framework.Structure
import de.pspaeth.kotco.framework.This
import javassist.CtMethod
import javassist.bytecode.AnnotationsAttribute
import javassist.bytecode.FieldInfo
import kotlin.reflect.jvm.jvmName
import kotlin.reflect.full.*


fun isInjecting(arr:List<Any>):Boolean {
	return arr.any{ c->
		(c as Annotation).annotationClass.isSubclassOf(Service::class) ||
		c.annotationClass.isSubclassOf(This::class) ||
		c.annotationClass.isSubclassOf(Structure::class)
	}
}


object MyAnnotations {
    fun transportAnnotations(fieldInfo:FieldInfo,origAnnotations:List<Any>) {
		val constp = fieldInfo.constPool
		val aa = AnnotationsAttribute(constp, AnnotationsAttribute.visibleTag)
		origAnnotations.forEach { fa ->
			when(fa) {
				is Service -> {
					val annot = javassist.bytecode.annotation.Annotation(Service::class.jvmName, constp)
					aa.addAnnotation(annot)									
				}
				is Structure -> {
					val annot = javassist.bytecode.annotation.Annotation(Structure::class.jvmName, constp)
					aa.addAnnotation(annot)																		
				}
				is This -> {
					val annot = javassist.bytecode.annotation.Annotation(This::class.jvmName, constp)
					aa.addAnnotation(annot)																		
				}
			}
		}
		fieldInfo.addAttribute(aa)
	}

	fun transportMethodAnnotations(method:CtMethod, annots:Array<Any>) {
		val constp = method.methodInfo.constPool
		val aa = AnnotationsAttribute(constp, AnnotationsAttribute.visibleTag)
		annots.forEach { fa ->
			when(fa) {
				is Defaults -> {
					val annot = javassist.bytecode.annotation.Annotation(Defaults::class.jvmName, constp)
					aa.addAnnotation(annot)									
				}
			}
		}
		method.methodInfo.addAttribute(aa)
	}
}