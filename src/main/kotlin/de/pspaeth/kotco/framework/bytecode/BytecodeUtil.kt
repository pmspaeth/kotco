package de.pspaeth.kotco.framework.bytecode

import javassist.ClassPool
import javassist.CtClass
import javassist.CtConstructor
import javassist.CtField
import javassist.CtMethod
import javassist.NotFoundException

object BytecodeUtil {
	val pool = ClassPool.getDefault()
	fun getMethod(c: CtClass, name: String, pars: Array<CtClass>): CtMethod? {
		try {
			return c.getDeclaredMethod(name, pars)
		} catch (e: NotFoundException) {
			return null
		}
	}

	fun getField(c: CtClass, name: String): CtField? {
		try {
			return c.getField(name)
		} catch (e: NotFoundException) {
			return null
		}
	}

	fun getCtClassByName(name: String): CtClass? {
		try {
			return pool.getCtClass(name)
		} catch (e: NotFoundException) {
			return null
		}
	}

	fun getConstructor(c: CtClass, descriptor: String): CtConstructor? {
		try {
			return c.getConstructor(descriptor)
		} catch (e: NotFoundException) {
			return null
		}
	}

	fun annonymousClassesNotPossible(typ: String, cc: CtClass) {
		cc.getRefClasses()?.filter { s -> s.toString().matches(Regex(".*\\$\\d+$")) }?.forEach { s ->
			throw RuntimeException("Due to JVM restrictions annonymous classes are not supported in ${typ} classes\n" +
					"You can however extract such classes into objects/classes outside the ${typ}\n" +
					"The ${typ} class is: ${cc.javaClass}\n" +
					"The offending annonymous class is: ${s}")
		}

	}
}