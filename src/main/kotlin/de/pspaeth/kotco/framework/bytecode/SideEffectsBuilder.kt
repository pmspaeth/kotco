package de.pspaeth.kotco.framework.bytecode

import de.pspaeth.kotco.framework.SideEffects
import javassist.CtClass
import javassist.CtMethod
import javassist.CtNewMethod
import kotlin.reflect.KClass
import kotlin.reflect.jvm.jvmName


class SideEffectsBuilder(val component:CtClass) {
	/**
	 * Using the composite declaration, add the side effects given in the SideEffects
	 * annotation. The side effect methods get inserted into the class with a
	 * "_SE1" , "_SE2", ... ending and a call to them gets added at the end of
	 * the corresponding mixed-in methods.
	 *
	 * Note that fields in side effects cannot be handled.
	 */
	fun <T:Any> handleSideEffects(declaring:KClass<T>) {
		var cnt = 0
		declaring.java.getAnnotation(SideEffects::class.java)?.clazzes?.forEach { kclazz ->
			cnt++
			val seClass = BytecodeUtil.getCtClassByName(kclazz.jvmName)?: throw RuntimeException("Internal error")
			
			BytecodeUtil.annonymousClassesNotPossible("sideeffect", seClass)
			
			seClass.getDeclaredMethods().forEach { m ->
				val methodName = m.name
				val parTypes = m.parameterTypes
				
				val m1:CtMethod? = BytecodeUtil.getMethod(component, methodName, parTypes)
				if(m1 == null) {
					val theCopyMethod = CtNewMethod.copy(m, component, null)
					m.annotations.takeIf { arr -> arr.isNotEmpty() }?.run { MyAnnotations.transportMethodAnnotations(theCopyMethod, this) }
					if(m.genericSignature != null) theCopyMethod.genericSignature = m.genericSignature
					component.addMethod(theCopyMethod)
				} else {
					component.getDeclaredMethod(methodName, parTypes)?.run {
						val seMethodCopy = CtNewMethod.copy(m, component, null).apply {
							name = methodName + "_SE${cnt}"
						}
						component.addMethod(seMethodCopy)
						insertAfter(methodName + "_SE${cnt}($$);", false)
    				}
				}
			}
			Fields.passFields(kclazz, component, cnt + 1000)
		}
	}	
}