package de.pspaeth.kotco.framework

import kotlin.reflect.KClass
import kotlin.reflect.KMutableProperty
import kotlin.reflect.full.allSuperclasses
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.staticProperties
import de.pspaeth.kotco.framework.*
import de.pspaeth.kotco.util.*
import kotlin.reflect.KFunction

/**
 * A repository for layers. Use it to build layers.
 */
object Application {
	val repo:MutableMap<String,Layer> = mutableMapOf()
	
	/**
	 * Build a layer
	 * @param name The layer name
	 */
	fun layer(name:String):Layer {
		return repo.getOrPut(name, { -> Layer(name)  } )
	}
}

/**
 * A framework layer. Do not use the constructor directly, instead use the `Application` object to build layers.
 */
class Layer(val name:String) {
	val transientRepo:MutableMap<KClass<*>,KClass<*>> = mutableMapOf()
	val transientNonUnique:MutableSet<KClass<*>> = mutableSetOf()
	val serviceRepo:MutableMap<KClass<*>,KClass<*>> = mutableMapOf()
	val serviceNonUnique:MutableSet<KClass<*>> = mutableSetOf()
	enum class Typ { TRANSIENT, SERVICE }

	/**
	 * Register transients
	 */	
	fun transients(vararg transients:KClass<*>) {
		buildAndRegister(Typ.TRANSIENT, name, *transients)
	}
	
	/**
	 * Register services
	 */
	fun services(vararg services:KClass<*>) {
		buildAndRegister(Typ.SERVICE, name, *services)
	}	

	/**
	 * Instantiate a transient
	 */
	fun <T:Any> newTransient(classOrInterface:KClass<T>):T {
		return newComposite(classOrInterface, Typ.TRANSIENT)
	}

	/**
	 * Instantiate a service. You normally don't use it, for services can be injected into fields.
	 */
	fun <T:Any> newService(classOrInterface:KClass<T>):T {
		return newComposite(classOrInterface, Typ.SERVICE)
	}
	
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	
	private fun <T:Any> newComposite(classOrInterface:KClass<T>, typ:Typ):T {
		val nonUnique = when(typ) {
			Typ.TRANSIENT -> transientNonUnique
			Typ.SERVICE -> serviceNonUnique }
		val repo = when(typ) {
			Typ.TRANSIENT -> transientRepo
			Typ.SERVICE -> serviceRepo }
		if(nonUnique.contains(classOrInterface))
			throw RuntimeException("Class/Interface ${classOrInterface} is not unique in layer ${name}" +
			"\nAvailable types:\n " +
			repo.keys.joinToString("\n "))
		if(!repo.containsKey(classOrInterface))
			throw RuntimeException("Class/Interface ${classOrInterface} does not exist in layer ${name}" +
			"\nAvailable types:\n " +
			repo.keys.joinToString("\n "))
		
		@Suppress("UNCHECKED_CAST")
		val theClass = repo[classOrInterface] as KClass<T>
		theClass.myStaticFields?.find({ sf -> sf.name == "_layer" })!!.run({ set(null, this@Layer) })
		
		try {
			val transObj = theClass.createInstance()
			return transObj
		} catch(e:InstantiationException) {
			System.err.println("Cannot instantiate ${classOrInterface}")
			System.err.println("Composite class = ${theClass}")
			theClass.members.filter({ m -> m is KFunction}).
					filter({ m -> (m as KFunction).isAbstract  }).
					forEach { m -> System.err.println("    Missing implementation for " + m) }
			throw e
		} catch(e:Exception) {
			e.printStackTrace(System.err)
			throw RuntimeException(e)
		}
	}	
	
	private fun buildAndRegister(typ:Typ, layerName:String, vararg clazz:KClass<*>) {
		val repo = when(typ) {
			Typ.TRANSIENT -> transientRepo
			Typ.SERVICE -> serviceRepo }
		val nonUnique = when(typ) {
			Typ.TRANSIENT -> transientNonUnique
			Typ.SERVICE -> serviceNonUnique }
		clazz.forEach { kk ->
			repo.putIfNotThere(kk, { -> when(typ) {
					Typ.SERVICE -> Factory.newService(kk, layerName)
					Typ.TRANSIENT -> Factory.newTransient(kk, layerName)
				} }	)
			kk.allSuperclasses.forEach({ k ->
				if(!nonUnique.contains(k)) {
					if(repo.containsKey(k)) {
						nonUnique.add(k)
						repo.remove(k)
					} else {
						repo[k] = repo[kk] as KClass<*>
					}
				}				
			})
		}
	}	

}