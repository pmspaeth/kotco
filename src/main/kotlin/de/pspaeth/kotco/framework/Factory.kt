package de.pspaeth.kotco.framework

import de.pspaeth.kotco.framework.bytecode.BodyAndMixinsBuilder
import de.pspaeth.kotco.framework.bytecode.BytecodeUtil
import de.pspaeth.kotco.framework.bytecode.ConcernsBuilder
import de.pspaeth.kotco.framework.bytecode.ConstraintsBuilder
import de.pspaeth.kotco.framework.bytecode.Enablers
import de.pspaeth.kotco.framework.bytecode.MyAnnotations
import de.pspaeth.kotco.framework.bytecode.SideEffectsBuilder
import de.pspaeth.kotco.framework.bytecode.StartComposite
import de.pspaeth.kotco.util.erasedType
import javassist.ClassPool
import javassist.CtClass
import javassist.CtNewMethod
import javassist.Modifier
import javassist.bytecode.Descriptor
import javassist.bytecode.SignatureAttribute
import kotlin.reflect.KClass
import kotlin.reflect.jvm.javaType
import kotlin.reflect.jvm.jvmName
import de.pspaeth.kotco.framework.bytecode.PropertiesBuilder

/**
 * An internal object for class manipulation.
 */
object Factory {
	val emptyConstructorDescriptor = Descriptor.ofConstructor(arrayOf())
	val pool = ClassPool.getDefault()
	
	fun <T:Any> newTransient(clazz:KClass<T>, layerName:String) : KClass<T> = newComposite(clazz, layerName)
	fun <T:Any> newService(clazz:KClass<T>, layerName:String) : KClass<T> = newComposite(clazz, layerName)
	
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	
	private fun <T:Any> newComposite(clazz:KClass<T>, layerName:String) : KClass<T> {
	    var compClass = StartComposite().prepareComposite(clazz, layerName)
		BodyAndMixinsBuilder(compClass).handleBodyAndMixins(clazz)
		ConstraintsBuilder(compClass).handleConstraints(clazz)
		SideEffectsBuilder(compClass).handleSideEffects(clazz)
		ConcernsBuilder(compClass).handleConcerns(clazz)
		
		Enablers(compClass).handleReferencedClasses(clazz)
		
		PropertiesBuilder(compClass).apply {
	    	handleDirectProperties(clazz)
	    	handleProperties()				
		}
		
		// Since we on the way temporarily added abstract methods and Javassist because of that
		// marks the class ABSTRACT, we must now remove that flag
		compClass.modifiers = compClass.modifiers and Modifier.ABSTRACT.inv()
		
		@Suppress("UNCHECKED_CAST")
		return compClass.toClass().kotlin as KClass<T>
	}
}