package de.pspaeth.kotco.framework

import kotlin.reflect.KFunction
import kotlin.reflect.full.*

/**
 * This used as a mixin will let toString() concatenate the properties of a composite
 */
abstract class ToStringByProperties {
	override fun toString():String {
		return "[" + this::class.simpleName?.replace(Regex("__tr__.*"),"") + ":" + this::class.members.filter{ m -> m is KFunction}.filter { m ->
			m.returnType.classifier == Property::class && m.name != "_theProperty"
		}.map{ m -> m.call(this)}.joinToString(",") + "]"
	}
}