package de.pspaeth.kotco.framework

fun <T> Property<T>.takeIfDefined(block: Property<T>.() -> Unit): Unit {
	//println(">>> " + (this as PropertyByMap).name + " " + isDefined())
	if(isDefined()) block()
}

/**
 * A property.
 */
interface Property<T> {
	fun get():T
	fun set(value:T)
	fun isDefined():Boolean
	fun ifDefined():T?
	fun unset()
}

class PropertyByMap<T>(val map:MutableMap<String,in T>, val name:String) : Property<T> {
	@Suppress("UNCHECKED_CAST") override fun get():T = map[name]!! as T
	override fun set(value:T) { map[name] = value }
	override fun isDefined() = map.containsKey(name)
	override fun unset() { map.remove(name) }
	@Suppress("UNCHECKED_CAST") override fun ifDefined() = map[name] as T?
	override fun toString():String { val v = map[name]?:"<undef>"; return "${name}=${v}" }
}

/** Use this as a mixin */
abstract class PropertyMapHandler {
	var _propertyMap:MutableMap<String,Any> = mutableMapOf<String,Any>()	
	@Suppress("UNUSED_PARAMETER")
	fun _theProperty(name:String, sign:String):Property<*> {
		return PropertyByMap(_propertyMap, name)
	}
	fun _thePropertyInit(name:String, sign:String) {
		if(sign.indexOf("Property<java.util.Set<") >= 0) {
			_propertyMap[name] = HashSet<Any>()
		} else if(sign.indexOf("Property<java.util.Map<") >= 0) {
			_propertyMap[name] = HashMap<Any, Any>()
		} else if(sign.indexOf("Property<java.util.List<") >= 0) {
			_propertyMap[name] = ArrayList<Any>()
		} else if(sign.indexOf("Property<java.lang.Integer") >= 0) {
			_propertyMap[name] = Integer(0)
		} else if(sign.indexOf("Property<java.lang.Double") >= 0) {
			_propertyMap[name] = 0.0
		} else if(sign.indexOf("Property<java.lang.Float") >= 0) {
			_propertyMap[name] = 0.0f
		} else if(sign.indexOf("Property<java.lang.Long") >= 0) {
			_propertyMap[name] = 0L
		} else if(sign.indexOf("Property<java.lang.Boolean") >= 0) {
			_propertyMap[name] = false
		} else if(sign.indexOf("Property<java.lang.Short") >= 0) {
			_propertyMap[name] = 0.toShort()
		} else if(sign.indexOf("Property<java.lang.Byte") >= 0) {
			_propertyMap[name] = 0.toByte()
		} else if(sign.indexOf("Property<java.lang.String") >= 0) {
			_propertyMap[name] = ""
		} else if(sign.indexOf("Property<java.lang.Character") >= 0) {
			_propertyMap[name] = ' '
		} else {
			_propertyMap[name] = Object()
		}
	}
}
