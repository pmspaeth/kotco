package de.pspaeth.kotco.framework

/**
 * A custom runtime exception used by constraints.
 */
class ConstraintViolationException(msg:String) : RuntimeException(msg)