package de.pspaeth.kotco.framework

import javassist.CtMethod
import javassist.bytecode.AnnotationsAttribute
import javassist.bytecode.FieldInfo
import kotlin.reflect.KClass
import kotlin.reflect.full.isSubclassOf
import kotlin.reflect.jvm.jvmName
import kotlin.jvm.*

/**
 * An annotation class to mark a field as a reference to a service. The service will be injected.
 */
@Target(AnnotationTarget.FIELD)
annotation class Service

/**
 * An annotation class to mark a field as a reference to a structure. Currently limited to the layer.
 * The reference will be injected. 
 */
@Target(AnnotationTarget.FIELD)
annotation class Structure

/**
 * An annotation class to mark a field as a reference to the composite itself. The value will be injected. 
 */
@Target(AnnotationTarget.FIELD)
annotation class This

/**
 * An annotation to be used for a mixin class. The parameters of this annotation must be
 * Kotlin object classes, which will then be eligible to have fields injected by the framework.
 */
@Target(AnnotationTarget.CLASS)
annotation class Enable(vararg val clazzes:KClass<out Any>)

/**
 * Use this as an annootation to a field inside an _enabled_ object (see the `Enable` annotation).
 * The field then gets the original mixin set as a reference
 */
@Target(AnnotationTarget.FIELD)
annotation class Enabler

/**
 * Add this annotatio to a property field to have it initialized with a default value
 */
annotation class Defaults

/**
 * Add this to a composite declaration to connect the composite to mixin classes.
 */
annotation class Mixins(vararg val clazzes:KClass<out Any>)

/**
 * Add this to a composite declaration to connect the composite to concern classes.
 */
annotation class Concerns(vararg val clazzes:KClass<out Any>)

/**
 * Add this to a composite declaration to connect the composite to side effect classes.
 */
annotation class SideEffects(vararg val clazzes:KClass<out Any>)

/**
 * Add this to a composite declaration to connect the composite to constraint classes.
 */
annotation class Constraints(vararg val clazzes:KClass<out Any>)
