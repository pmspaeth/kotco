package de.pspaeth.kotco.framework

import java.lang.reflect.Field
import java.lang.reflect.Modifier
import java.util.Arrays
import kotlin.reflect.KClass

/**
 * Add a static fields accessor to `KClass`
 */
var KClass<*>.myStaticFields:Array<Field>?
	get() {
		// This looks a little bit complicated, but the staticFields accessor of Kotlin
		// has a problem (a bug?)
		val l = mutableListOf<Field>()
		val arr = this.java.fields
		for(i in 0 until arr.size) {
			if(arr[i].modifiers and Modifier.STATIC != 0) l.add(arr[i])
		}
		return l.toTypedArray()
	}
    set(_) { throw UnsupportedOperationException() }
