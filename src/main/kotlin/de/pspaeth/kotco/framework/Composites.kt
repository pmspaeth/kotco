package de.pspaeth.kotco.framework

interface TransientComposite

interface ServiceComposite

/**
 * Mark a class as a concern. 
 */
interface Concern {
	fun callOrig(vararg par:Any):Any
}