package de.pspaeth.kotco.configuration

import org.apache.commons.configuration2.Configuration
import org.apache.commons.configuration2.FileBasedConfiguration
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder
import org.apache.commons.configuration2.builder.fluent.Parameters
import org.apache.commons.configuration2.ex.ConfigurationException
import org.apache.commons.configuration2.PropertiesConfiguration as PConf

/**
 * An implementation for the `ConfService` using the `config.properties` file found in the current
 * working directory.
 *
 * Use it as a mixin. For example:
 * 
 *     @Mixins(PropertiesConfiguration::class)
 *     abstract class StdConfService : ConfigurationComposite()
 *
 * You can then register this service inside the layer and henceforth use it to access properties
 */
abstract class PropertiesConfiguration : ConfService{
	var config:Configuration
	init {
		val params = Parameters()
		val builder = FileBasedConfigurationBuilder<FileBasedConfiguration>(PConf::class.java)
			.configure(params.properties().setFileName( getPropertiesFileName()))
		try {
			config = builder.configuration
		} catch(e:ConfigurationException) {
			throw RuntimeException("loading of the configuration file failed", e)
		}
	}
	
	/**
	 * Override this to provide a different properties file name.
	 */
	open fun getPropertiesFileName() = "config.properties"
	
	/**
	 * Retrieve an integer value
	 */
	override fun intVal(key:String):Int {
		return config.getInt(key)
	}
	
	/**
	 * Retrieve an integer value, using a default if the key doesn't exist
	 */
	override fun intVal(key:String, defau:Int):Int {
		return config.getInt(key, defau)
	}
	
	/**
	 * Retrieve a double value
	 */
	override fun doubleVal(key:String):Double {
		return config.getDouble(key)
	}
	
	/**
	 * Retrieve an double value, using a default if the key doesn't exist
	 */
	override fun doubleVal(key:String, defau:Double):Double {
		return config.getDouble(key, defau)
	}
	
	/**
	 * Retrieve a string value
	 */
	override fun stringVal(key:String):String {
		return config.getString(key)
	}
	
	/**
	 * Retrieve an string value, using a default if the key doesn't exist
	 */
	override fun stringVal(key:String, defau:String):String {
		return config.getString(key, defau)
	}
	
	/**
	 * Retrieve a boolean value (use "true" or "false" inside the properties file)
	 */
	override fun boolVal(key:String):Boolean {
		return config.getBoolean(key)
	}
	
	/**
	 * Retrieve a boolean value, using a default if the key doesn't exist (use "true"
	 * or "false" inside the properties file)
	 */
	override fun boolVal(key:String, defau:Boolean):Boolean {
		return config.getBoolean(key, defau)
	}
	
	/**
	 * Retrieve an int array. This corresponds to a comma separated list of integer
	 * values in the properties file:
	 *
	 *     some.key: 56, 3, 0, 2, -5
	 */
	override fun intArray(key:String) : IntArray {
		val s = config.getString(key)
		return s.split(Regex(",")).map({ it.trim().toInt() } ).toIntArray()
	}
	
	/**
	 * Retrieve a double array. This corresponds to a comma separated list of double
	 * values in the properties file:
	 *
	 *     some.key: 56.7, 3, 0.6, 2.7, -5.77
	 */
	override fun doubleArray(key:String) : DoubleArray {
		val s = config.getString(key)
		return s.split(Regex(",")).map({ it.trim().toDouble() } ).toDoubleArray()
	}
}