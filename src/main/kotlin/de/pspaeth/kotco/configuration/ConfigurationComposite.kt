package de.pspaeth.kotco.configuration

import de.pspaeth.kotco.framework.Mixins

/**
 * A base class to be used for configuration composites. Inside your application, derrive from this and
 * for example write
 *
 *     @Mixins(PropertiesConfiguration::class)
 *     abstract class StdConfService : ConfigurationComposite()
 *
 * You can then add this as a service to your layer:
 * 
 *     layer.services(StdConfService::class)
 *
 */
@Mixins(ConfService::class)
abstract class ConfigurationComposite : ConfService