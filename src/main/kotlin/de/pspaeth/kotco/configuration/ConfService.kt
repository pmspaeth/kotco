package de.pspaeth.kotco.configuration

/**
 * A configuration service. You can use
 * 
 *     @Service lateinit var conf:ConfService
 *
 * To inject a service implementing this interface. You then need to provide a configuration implementation.
 * One possible way for this is to build a composite as follows:
 *  
 *     @Mixins(PropertiesConfiguration::class)
 *     abstract class StdConfService : ConfigurationComposite()
 * 
 * and then register it inside the layer:
 * 
 *     layer.services(StdConfService::class)
 * 
 */
interface ConfService {
	fun intVal(key:String):Int
	fun intVal(key:String, defau:Int):Int
	fun doubleVal(key:String):Double
	fun doubleVal(key:String, defau:Double):Double
	fun stringVal(key:String):String
	fun stringVal(key:String, defau:String):String
	fun boolVal(key:String):Boolean
	fun boolVal(key:String, defau:Boolean):Boolean
	fun intArray(key:String) : IntArray
	fun doubleArray(key:String) : DoubleArray
}