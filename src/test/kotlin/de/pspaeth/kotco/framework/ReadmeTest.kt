package de.pspaeth.kotco.framework

import org.junit.Assert.*
import org.junit.Test

interface Person : HasName, HasYearOfBirth
interface HasName { fun getName():String }
interface HasYearOfBirth { fun getYearOfBirth():Int }

abstract class SimpleName : Person {
    override fun getName():String {
        // In reality request some backend service...
        return "John Doe"
    }
}

abstract class SimpleYearOfBirth : Person {
    override fun getYearOfBirth():Int {
        // In reality request some backend service...
        return 1997
    }
}

@Mixins(SimpleName::class, SimpleYearOfBirth::class)
abstract class PersonTransient : Person

class ReadmeTest {

	@Test
    fun testLayers() {
		val layer = Application.layer("TheLayer").apply {
			transients(PersonTransient::class)
		}
	}
	
	//////////////////////////////////////////////////////////////////////////////
	
	@Test
	fun testMixins() {
		val layer = Application.layer("TheLayer").apply {
			transients(PersonTransient::class)
		}
		val pers = layer.newTransient(PersonTransient::class)
		assertEquals( 1997, pers.getYearOfBirth() )
	}
	
	//////////////////////////////////////////////////////////////////////////////

    interface AddingInterface { fun add(a:Int,b:Int):Int }
    abstract class Mixin : AddingInterface { override fun add(a:Int,b:Int):Int = a + b }
    abstract class TheConcern : Concern, AddingInterface {
        override fun add(a:Int,b:Int):Int {
            val c = callOrig(a, b + 1) as Int
            return c + 1
        }
    }
	@Mixins(Mixin::class)
    @Concerns(TheConcern::class)
    abstract class TheComposite : AddingInterface

	@Test
	fun testConcerns() {
		val layer = Application.layer("TheLayer").apply {
			transients(TheComposite::class)
		}
		val compo = layer.newTransient(TheComposite::class)
		assertEquals( 11, compo.add(3,6) )
    
	}

	//////////////////////////////////////////////////////////////////////////////
	
	companion object { var x7:Int = 0; var q42:String = "" }
    abstract class NameSideEffect : HasName {
        override fun getName():String {
            // For example do some logging...
            // ...
			x7 = 7
            return "" // Nobody cares
        }
    }

	
	@Test
	fun testSideEffects() {
		@Mixins(SimpleName::class, SimpleYearOfBirth::class)
        @SideEffects(NameSideEffect::class)
        abstract class PersonTransient : Person

		val layer = Application.layer("TheLayer").apply {
			transients(PersonTransient::class)
		}
		val pers = layer.newTransient(PersonTransient::class)
		pers.getName()
		
		assertEquals( 7, x7 )
	}
	
	//////////////////////////////////////////////////////////////////////////////
	
	interface i1 {
		fun a(par:Int):Int
		fun b(par:Int):Int
	}
	abstract class I1Mixin {
		fun a(par:Int):Int = par + 7
		fun b(par:Int):Int = par + 42
	}
	abstract class Constraint1 : i1 {
		override fun a(par:Int):Int {
			if(par < 0) throw ConstraintViolationException("Param must be >= 0")
			return 0 // nobody cares
		}
	}	

	@Test
	fun testConstraints() {
		@Mixins(I1Mixin::class)
		@Constraints(Constraint1::class)
		abstract class TheTransient : i1
		
		val layer = Application.layer("TheLayer").apply {
			transients(TheTransient::class)
		}
		val compo = layer.newTransient(TheTransient::class)
		var thrown = false
		try { compo.a(-5) } catch(e:ConstraintViolationException){ thrown = true }
		assertTrue(thrown)
		
		compo.a(42)
	}
	
	//////////////////////////////////////////////////////////////////////////////

	interface PPerson : PHasName, PHasYearOfBirth
    interface PHasName { fun name():Property<String> }
    interface PHasYearOfBirth { fun yearOfBirth():Property<Int> }
    abstract class PSimpleName : PPerson {
        @Defaults override abstract fun name():Property<String>
    }    
    abstract class PSimpleYearOfBirth : PPerson {
        @Defaults override abstract fun yearOfBirth():Property<Int>
    }

	@Test
	fun testProperties() {
		@Mixins(PropertyMapHandler::class, PSimpleName::class, PSimpleYearOfBirth::class)
		abstract class PersonTransient : PPerson

		val layer = Application.layer("TheLayer").apply {
			transients(PersonTransient::class)
		}
		val person = layer.newTransient(PersonTransient::class)
		person.name().set("Jean")
		assertEquals("Jean", person.name().get())
	}	

	//////////////////////////////////////////////////////////////////////////////

	interface s1 { fun run(par: Int):String }

	class ServiceMixin : s1 {
		override fun run(par: Int):String {
			return "Service run(${par})"
		}
	}
	
	@Mixins(ServiceMixin::class)
	abstract class ServiceComp : ServiceComposite, s1
	
	@Mixins(ServiceClientCompo.Mixin::class)
	abstract class ServiceClientCompo {
		abstract fun a()
		abstract class Mixin {
			@Service lateinit var servc:s1
			fun a(){
				q42 = servc.run(42)
			}
		}
	}
	
	@Test
	fun testServices() {
		val layer = Application.layer("TheLayer")
		layer.transients(ServiceClientCompo::class)
		layer.services(ServiceComp::class)
		val client = layer.newTransient(ServiceClientCompo::class)
		
		q42 = ""
		client.a()
		assertEquals("Service run(42)", q42)
		
	}

	//////////////////////////////////////////////////////////////////////////////
    
    abstract class SimpleName2 {
        @This lateinit var thePerson:Person 
        @Structure lateinit var layer:Layer 
        fun getName():String = "John from inside ${layer} ${thePerson}" 
    }
    
	@Test
	fun testInjections() {
		// Services were already tested
		
	    @Mixins(SimpleName2::class, SimpleYearOfBirth::class)
	    abstract class PersonComposite : Person

		val layer = Application.layer("TheLayer")
		layer.transients(PersonComposite::class)
		val perso = layer.newTransient(PersonComposite::class)
		
		val reg = Regex("John from inside de.pspaeth.kotco.framework.Layer@.* de.pspaeth.kotco.framework.ReadmeTest.*testInjections.*PersonComposite__tr__TheLayer@.*")
		assertTrue(perso.getName().matches(reg))

	}
	
	//////////////////////////////////////////////////////////////////////////////

	object SomeObject {
		@Enabler lateinit var enabler:Person
		fun a():String {
			return enabler.getName() + " from SomeObject"
		}
	}
	
	@Test
	fun testEnablers() {
		@Enable(SomeObject::class)
		abstract class SimpleYearOfBirth3 : Person {
			override fun getYearOfBirth():Int {
				q42 = SomeObject.a()
				return 1997
			}
		}

		@Mixins(SimpleName::class, SimpleYearOfBirth3::class)
	    abstract class PersonComposite : Person

		val layer = Application.layer("TheLayer")
		layer.transients(PersonComposite::class)
		val perso = layer.newTransient(PersonComposite::class)
		
		q42 = ""
		perso.getYearOfBirth()
		assertEquals(q42, "John Doe from SomeObject")
	}
		
		
}