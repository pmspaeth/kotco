package de.pspaeth.kotco.framework

import org.junit.Assert.assertTrue
import org.junit.Test
import org.junit.BeforeClass


class InjectionTest {

	interface i1 {
		fun a(par: Int): String
		fun b(par: Int): Int
		fun c(): String
	}

	interface s1 {
		fun run(par: Int):String
	}

	abstract class ServiceComp : s1 {
		override fun run(par: Int):String {
			return "Service run(${par})"
		}
	}

	abstract class MixinClass1 : i1 {
		override fun b(par: Int) = par + 7
	}

	abstract class MixinClass2 : i1 {
		@Service
		lateinit var servc: ServiceComp
		
		@Structure
		lateinit var layer:Layer
		
		@This
		lateinit var me1:i1

		override fun a(par: Int): String {
			return servc.run(par) + ":" + layer
		}
		
		override fun c(): String {
			return me1.toString()
		}
	}

	@Mixins(MixinClass1::class, MixinClass2::class)
	abstract class TheTransient : i1
	
	@Test
	fun structure() {
		var layer = Application.layer("TheLayer")
		layer.transients(TheTransient::class)
		layer.services(ServiceComp::class)
	
		val tr = layer.newTransient(TheTransient::class)
	    assertTrue(tr.a(7).startsWith("Service run(7):de.pspaeth.kotco.framework.Layer@") )		
	}
	
	@Test
	fun thiss() {
		var layer = Application.layer("TheLayer")
		layer.transients(TheTransient::class)
		layer.services(ServiceComp::class)
	
		val tr = layer.newTransient(TheTransient::class)
		assertTrue(tr.c().startsWith("de.pspaeth.kotco.framework.InjectionTest\$TheTransient__tr__TheLayer@"))		
	}


}