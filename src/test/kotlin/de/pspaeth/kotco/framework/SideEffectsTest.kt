package de.pspaeth.kotco.framework

import org.junit.Assert.*
import org.junit.Test


class SideEffectsTest {
	interface i1 {
		fun a(par:Int):Int
		fun b(par:Int):Int
	}

	companion object { var x = 0 }
	abstract class SideEffect1 : i1 {
		override fun a(par:Int):Int {
			x = 42
			return 0 // noboy cares
		}
	}	

	@Test
    fun sideEffect() {
		
		abstract class Mixin1 : i1 {
			override fun a(par:Int):Int = par + 42
		}	

		abstract class Mixin2 : i1 {
			override fun b(par:Int):Int = par + 7		
		}	

		@Mixins(Mixin1::class, Mixin2::class)
		@SideEffects(SideEffect1::class)
		abstract class TheTransient : i1
		
		// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		// SideEffects NOT inside functions (Kotlin or Javassist bug)

		val layer = Application.layer("TheLayer")
        layer.transients(TheTransient::class)
        val tr = layer.newTransient(TheTransient::class)
		val res = tr.a(11)
		assertEquals(53, res)
		assertEquals(42, x)
	}
}