package de.pspaeth.kotco.framework

import org.junit.Assert.*
import org.junit.Test


class ConstraintsTest {
	interface i1 {
		fun a(par:Int):Int
		fun b(par:Int):Int
	}

	abstract class Constraint1 : i1 {
		override fun a(par:Int):Int {
			if(par < 0) throw ConstraintViolationException("Param must be >= 0")
			return 0 // noboy cares
		}
	}	

	@Test
    fun sideEffect() {
		
		abstract class Mixin1 : i1 {
			override fun a(par:Int):Int = par + 42
		}	

		abstract class Mixin2 : i1 {
			override fun b(par:Int):Int = par + 7		
		}	

		@Mixins(Mixin1::class, Mixin2::class)
		@Constraints(Constraint1::class)
		abstract class TheTransient : i1
		
		val layer = Application.layer("TheLayer")
        layer.transients(TheTransient::class)
        val tr = layer.newTransient(TheTransient::class)
		tr.a(11)
		
		var thrown = false
		try {
			tr.a(-1)
		}catch(e:ConstraintViolationException) {
			thrown = true
		}
		assertTrue(thrown)
	}
}