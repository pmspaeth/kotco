package de.pspaeth.kotco.framework

import org.junit.Assert.*
import org.junit.Test

class ConcernsTest {
	interface i1 {
		fun a(par:Int):Int
		fun b(par:Int):Int
	}

	@Test
    fun concern() {
		abstract class Mixin1 : i1 {
			override fun a(par:Int):Int = par + 42
		}	

		abstract class Mixin2 : i1 {
			override fun b(par:Int):Int = par + 7		
		}	

		abstract class Concern1 : Concern, i1 {
			override fun a(par:Int):Int {
				return (callOrig(par + 3) as Int + 100)
			}
		}	

		@Mixins(Mixin1::class, Mixin2::class)
		@Concerns(Concern1::class)
		abstract class TheTransient : i1

		val layer = Application.layer("TheLayer")
        layer.transients(TheTransient::class)
        val tr = layer.newTransient(TheTransient::class)
		val res = tr.a(11)
		assertEquals(156, res)
	}
	
	@Test
    fun concernX2() {
		abstract class Mixin1 : i1 {
			override fun a(par:Int):Int = par + 42
		}	

		abstract class Mixin2 : i1 {
			override fun b(par:Int):Int = par + 7		
		}	

		abstract class Concern1 : Concern, i1 {
			override fun a(par:Int):Int {
				return (callOrig(par + 3) as Int + 100)
			}
		}	

		abstract class Concern2 : Concern, i1 {
			override fun a(par:Int):Int {
				return (callOrig(par) as Int + 1000)
			}
		}	

		@Mixins(Mixin1::class, Mixin2::class)
		@Concerns(Concern1::class, Concern2::class)
		abstract class TheTransient : i1

		val layer = Application.layer("TheLayer")
        layer.transients(TheTransient::class)
        val tr = layer.newTransient(TheTransient::class)
		val res = tr.a(11)
		assertEquals(1156, res)
	}
	
}