package de.pspaeth.kotco.framework

import org.junit.Assert.*
import org.junit.Test

class MixinTest {
	interface i1 {
		fun a(par:Int):Int
		fun b(par:Int):Int
	}

	@Test
    fun noMixin() {
		abstract class NoMixinTransient : i1	

		val layer = Application.layer("TheLayer")
        layer.transients(NoMixinTransient::class)
        val tr = layer.newTransient(NoMixinTransient::class)
		
		var errThrown = false
		try {tr.a(7)} catch(e:AbstractMethodError) { errThrown = true}
		assertTrue("Expected AbstractMethodError not thrown", errThrown)
	}
	
	@Test
	fun allMixinsSimple() {
		abstract class SimpleMixinClass1 : i1 {
			override fun b(par:Int) = par + 7
		}
		
		abstract class SimpleMixinClass2 : i1 {
			override fun a(par:Int) = par + 8
		}

		@Mixins(SimpleMixinClass1::class, SimpleMixinClass2::class)
		abstract class TheTransient : i1
		
        val layer = Application.layer("TheLayer")
        layer.transients(TheTransient::class)
        val tr = layer.newTransient(TheTransient::class)
		assertEquals(15, tr.a(7))
	}

	@Test
	fun mixinsWithFields() {
		abstract class SimpleMixinClass1 : i1 {
			val abc = 3
			override fun b(par:Int) = par + 7 + abc
		}
		
		abstract class SimpleMixinClass2 : i1 {
			override fun a(par:Int) = par + 8
		}

		@Mixins(SimpleMixinClass1::class, SimpleMixinClass2::class)
		abstract class TheTransient : i1
		
        val layer = Application.layer("TheLayer")
        layer.transients(TheTransient::class)
        val tr = layer.newTransient(TheTransient::class)
		assertEquals(17, tr.b(7))
	}

	abstract class MixinWithStatic : i1 {
			companion object { val abc = 3 }
			override fun b(par:Int) = par + 7 + abc
	}
	
	@Test
	fun mixinsWithStaticFields() {
		abstract class SimpleMixinClass2 : i1 {
			override fun a(par:Int) = par + 8
		}

		@Mixins(MixinWithStatic::class, SimpleMixinClass2::class)
		abstract class TheTransient : i1
		
        val layer = Application.layer("TheLayer")
        layer.transients(TheTransient::class)
        val tr = layer.newTransient(TheTransient::class)
		assertEquals(17, tr.b(7))
	}

	abstract class MixinWithVarStatic : i1 {
			companion object { var abc = 3 }
			override fun b(par:Int) = par + 7 + abc
	}
	
	@Test
	fun mixinsWithVarStaticFields() {
		abstract class SimpleMixinClass2 : i1 {
			override fun a(par:Int) = par + 8
		}

		@Mixins(MixinWithVarStatic::class, SimpleMixinClass2::class)
		abstract class TheTransient : i1
		
        val layer = Application.layer("TheLayer")
		var correctlyThrown = false
		try{
            layer.transients(TheTransient::class)
		}catch(e:RuntimeException){
			correctlyThrown = e.message?.startsWith("Companion var fields not allowed, in mixed-in class")?:false
		}
		assertTrue(correctlyThrown)
	}
	
	@Test
	fun noAnnonymousClasses() {
		abstract class Annonym1 : i1 {
			val annon = { b:Int -> b + 7 }
		}
		
		@Mixins(Annonym1::class)
		abstract class TheTransient : i1

        val layer = Application.layer("TheLayer")
		var correctlyThrown = false
		try{
            layer.transients(TheTransient::class)
		}catch(e:RuntimeException){
			correctlyThrown = e.message?.startsWith("Due to JVM restrictions annonymous classes are not supported in mixin classes")?:false
		}
		assertTrue(correctlyThrown)
		
	}

}