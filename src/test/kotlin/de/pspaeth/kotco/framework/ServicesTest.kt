package de.pspaeth.kotco.framework

import org.junit.Assert.assertEquals
import org.junit.Test

class ServicesTest {
	interface i1 {
		fun a(par: Int): String
		fun b(par: Int): Int
	}

	interface s1 {
		fun run(par: Int):String
	}

	class ServiceMixin : s1 {
		override fun run(par: Int):String {
			return "Service run(${par})"
		}
	}

	@Mixins(ServiceMixin::class)
	abstract class ServiceComp : ServiceComposite, s1
	
	abstract class MixinClass1 : i1 {
		override fun b(par: Int) = par + 7
	}

	abstract class MixinClass2 : i1 {
		@Service
		lateinit var servc: ServiceComp

		override fun a(par: Int): String {
			return servc.run(par)
		}
	}

	@Mixins(MixinClass1::class, MixinClass2::class)
	abstract class TheTransient : i1

	@Test fun services() {
		val layer = Application.layer("TheLayer")
		layer.transients(TheTransient::class)
		layer.services(ServiceComp::class)
	
		val tr = layer.newTransient(TheTransient::class)
		assertEquals("Service run(7)", tr.a(7))
	}
}