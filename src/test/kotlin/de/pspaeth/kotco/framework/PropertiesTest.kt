package de.pspaeth.kotco.framework

import org.junit.Assert.*
import org.junit.Test

class PropertiesTest {

	interface I {
		fun a(v: Int): Int
	}

	abstract class M1 : I {
		abstract fun b(): Property<Int>
		override fun a(v: Int): Int {
			b().set(42); return v + b().get() + 7
		}
	}

	abstract class M2 : I {
		abstract fun b(): Property<Int>
		override fun a(v: Int): Int {
			return v + b().get() + 7
		}
	}

	abstract class M3 : I {
		@Defaults abstract fun b(): Property<Int>
		override fun a(v: Int): Int {
			return v + b().get() + 7
		}
	}

	@Mixins(M1::class, PropertyMapHandler::class)
	abstract class Transient : I

	@Mixins(M2::class, PropertyMapHandler::class)
	abstract class Transient2 : I

	@Mixins(M3::class, PropertyMapHandler::class)
	abstract class Transient3 : I
	
	@Mixins(PropertyMapHandler::class)
	abstract class Transient4 : I {
		@Defaults abstract fun b(): Property<Int>
		override fun a(v: Int): Int {
			return v + b().get() + 7
		}		
	}
	

	@Test
	fun property() {
		val lay = Application.layer("TheLayer")
		lay.transients(Transient::class)

		val t = lay.newTransient(Transient::class)
		assertEquals(56, t.a(7))
	}
	
	@Test
	fun propertyInitializer() {
		val lay = Application.layer("TheLayer")
		lay.transients(Transient2::class)

		val t = lay.newTransient(Transient2::class)
		var correctlyThrown = false
		try {
			assertEquals(14, t.a(7))
		}catch(e:KotlinNullPointerException) {
			correctlyThrown = true
		}
		assertTrue(correctlyThrown)
		
		lay.transients(Transient3::class)
		val t2 = lay.newTransient(Transient3::class)
		assertEquals(14, t2.a(7))
	}
	
	@Test
    fun allInDeclaring() {
		val lay = Application.layer("TheLayer")
		lay.transients(Transient4::class)
		val t = lay.newTransient(Transient4::class)
		assertEquals(14, t.a(7))		
	}
	
}